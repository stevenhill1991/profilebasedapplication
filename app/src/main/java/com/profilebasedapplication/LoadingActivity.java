package com.profilebasedapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.profilebasedapplication.home.HomeActivity;
import com.profilebasedapplication.setup.SetupActivity;

public class LoadingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        Context context = LoadingActivity.this;

        int[] logosArray = {
            R.drawable.rslogo,
            R.drawable.osrslogo,
            R.drawable.rstoolslogo
        };

        drawLogos(logosArray);

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        initializeStartup();
                    }
                },
                25000);
    }

    public void drawLogos(int[] logos){
        if(logos.length <= 0) {
            return;
        }

        final int[] imageArr = logos;

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {

            Context context = LoadingActivity.this;
            ImageView iv = (ImageView) findViewById(R.id.loadingImageView);
            Animation fadeIn = AnimationUtils.loadAnimation(context, R.anim.fadein);
            Animation fadeOut = AnimationUtils.loadAnimation(context, R.anim.fadeout);

            Animation.AnimationListener animationListener = new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    Log.v("STARTING FADE OUT", "FADE OUT");
                    iv.startAnimation(fadeOut);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            };

            int i = 0;

            public void run() {
                fadeIn.setAnimationListener(animationListener);
                iv.setImageResource(imageArr[i]);
                iv.startAnimation(fadeIn);
                i++;

                if(i <= imageArr.length-1){
                    handler.postDelayed(this, 5000);
                } else {
                    fadeIn.setAnimationListener(null);
                }

            }

        };
        handler.postDelayed(runnable,2000);

    }

    public void initializeStartup(){
        Context context = LoadingActivity.this;

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        String userName = sharedPref.getString(getString(R.string.saved_userName),null);

        if (userName != null && !userName.isEmpty()) {
            Intent i = new Intent(context, SetupActivity.class);
            finish();
            startActivity(i);
        }

        Intent i = new Intent(context, HomeActivity.class);
        finish();
        startActivity(i);
    }

}
